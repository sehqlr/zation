import Text.ParserCombinators.ReadP
import Control.Applicative

headline :: ReadP String
headline = do
  stars <- many1 (satisfy (\char -> char == '*'))
  string " "
  return stars

comment :: ReadP String
comment = do
  string "#"

command :: ReadP String
command = do
  bol <- comment
  pls <- string "+"
  return (bol ++ pls)

todo :: ReadP String
todo = do
  status <- string "TODO" <|> string "DONE"
  string " "
  return status

todoItem :: ReadP String
todoItem = do
  headline
  todo
