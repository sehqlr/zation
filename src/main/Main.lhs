> {-# LANGUAGE OverloadedStrings #-}
> module Main (main) where

import the zation library and CLI dependencies

> import Zation

> import System.Environment
> import Text.Pandoc
> import Text.Pandoc.Walk
> import qualified Data.Text.IO as TIO
> import qualified Data.Map.Strict as Map

read in a org-mode file, then show the data structure, then exit

> main :: IO ()
> main = do
>   f:_ <- getArgs
>   f' <- TIO.readFile f
>   result <- runIO $ do
>     txt <- readOrg def f'
>     writeNative def txt
>   org <- handleError result
>   TIO.putStrLn org




