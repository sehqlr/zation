> {-# LANGUAGE OverloadedStrings #-}
> module Zation
>          (Document
>          , options
>          , outline
>          , OutlineTree) where

> import qualified Data.Text as T
> import qualified Data.Map.Strict as Map
> import Data.Tree
> import Text.Pandoc.Definition

TYPES

> data Document = Document Properties OutlineTree deriving (Show, Read)

> options :: Document -> Properties
> options (Document o _) = o

> outline :: Document -> OutlineTree
> outline (Document _ o) = o

> data Headline = Headline
>                 { keyword    :: Keyword
>                 , priority   :: Char
>                 , title      :: T.Text
>                 , tags       :: [ T.Text ]
>                 , properties :: Properties
>                 , section    :: [ T.Text ]
>                 } deriving (Show, Read)

> empty :: Headline
> empty = Headline
>         { keyword    = TODO
>         , priority   = 'B'
>         , title      = "test"
>         , tags       = []
>         , properties = Map.empty
>         , section    = []
>         }

> type Properties = Map.Map T.Text T.Text

> type OutlineTree = Tree Headline

> data Keyword = TODO
>              | DONE
>              deriving (Show, Read)

> fromPandoc :: Pandoc -> Document
> fromPandoc ( Pandoc meta blocks ) = let properties = Map.empty
>                                         outlineTree = convertBlocks blocks (Node { rootLabel = empty, subForest = []})
>                                     in Document properties outlineTree

> convertBlocks :: [Block] -> OutlineTree -> OutlineTree
> convertBlocks (b:bs) soFar = soFar
